<?php

//Recebe um valor e retorna true caso o ano seja bissexto
function isBissexto($number)
{

	if (($number % 4 == 0) && ($number % 100 != 0 || $number %400 == 0)) {
		return true;
	} else {
		return false;
	}

}

//Recebe o mês e o ano e retorna o número de dias do mês
function getDaysFromMonth($month, $year)
{
    if ($month == 2 & isBissexto($year)) {
        return 29;
    }

    if ($month == 2 & !isBissexto($year)) {
        return 28;
    }

    if ($month == 4 || $month == 6 || $month == 9 || $month == 11) {
        return 30;
    }else{
        return 31;
    }
}

//Vai retornar a diferença entre os meses sem o mês inicial e o final
//Exemplo: 2020-05 e 2020-08 -> vai retornar 2020-06, 2020-07
function diferenceBetweenMonths($initialDate, $finalDate)
{
    $initialDate = explode("-",$initialDate);
    $finalDate = explode("-",$finalDate);

    $anoI = intval($initialDate[0]);
    $anoF = intval($finalDate[0]);
    $mesI = intval($initialDate[1]);
    $mesF = intval($finalDate[1]);

    $diferenceAno = $anoF - $anoI;

    $total = ($diferenceAno * 12)  +  ($mesF - $mesI - 1);

    if($mesF - $mesI == 1 && $anoF == $anoI){
        $total = 1;
    }


    $countMeses = [];
    $month = $mesI + 1;

    $new = 1;
    for ($x = 1; $x <= $total; $x++) {
       
        array_push($countMeses, strval($anoI.'-'.$month));
        
        $month++;

        if ($month == 13) {
            $month = $new;
            $anoI++;
        }

      }
    return $countMeses;

}


/**
* Calcula o numero de dias entre 2 datas.
* As datas passadas sempre serao validas e a primeira data sempre sera menor que a segunda data.
* @param string $dataInicial No formato YYYY-MM-DD
* @param string $dataFinal No formato YYYY-MM-DD
* @return int O numero de dias entre as datas
**/
function calculaDias($dataInicial, $dataFinal) {
	$meses = diferenceBetweenMonths($dataInicial, $dataFinal);
    $dataI = explode('-',$dataInicial);
    $dataF = explode('-',$dataFinal);
    $dias = 0;
    //print_r($meses);

	//Somando os dias dos meses completos
    foreach ($meses as $m) {
        $data = explode('-',$m);
        $ano = $data[0];
        $mes = $data[1];
        $dias += getDaysFromMonth($mes, $ano);  
        
    }


    if ($dataI[0] == $dataF[0] && $dataI[1] == $dataF[1]) { //Se for mesmo ano e mês, apenas calcula a diferença dos dias
        $dias = $dataF[2] - $dataI[2];
    }else{

        if(intval($dataF[1]) - intval($dataI[1]) == 1 && $dataI[0] == $dataF[0]){
            
            $diasIniciais = getDaysFromMonth($dataI[1], $dataI[0]);
            $dias =  $diasIniciais - $dataI[2];
            $dias +=  $dataF[2];
        }else{
              
            $diasIniciais = getDaysFromMonth($dataI[1], $dataI[0]);
            $dias +=  $diasIniciais - $dataI[2];
            $dias +=  $dataF[2];
        }

    }
   
   return $dias;
    
    
}



/***** Teste 01 *****/
$dataInicial = "2018-01-01";
$dataFinal = "2018-01-02";
$resultadoEsperado = 1;
$resultado = calculaDias($dataInicial, $dataFinal);
verificaResultado("01", $resultadoEsperado, $resultado);

/***** Teste 02 *****/
$dataInicial = "2018-01-01";
$dataFinal = "2018-02-01";
$resultadoEsperado = 31;
$resultado = calculaDias($dataInicial, $dataFinal);
verificaResultado("02", $resultadoEsperado, $resultado);

/***** Teste 03 *****/
$dataInicial = "2018-01-01";
$dataFinal = "2018-02-02";
$resultadoEsperado = 32;
$resultado = calculaDias($dataInicial, $dataFinal);
verificaResultado("03", $resultadoEsperado, $resultado);

/***** Teste 04 *****/
$dataInicial = "2018-01-01";
$dataFinal = "2018-02-28";
$resultadoEsperado = 58;
$resultado = calculaDias($dataInicial, $dataFinal);
verificaResultado("04", $resultadoEsperado, $resultado);

/***** Teste 05 *****/
$dataInicial = "2018-01-15";
$dataFinal = "2018-03-15";
$resultadoEsperado = 59;
$resultado = calculaDias($dataInicial, $dataFinal);
verificaResultado("05", $resultadoEsperado, $resultado);

/***** Teste 06 *****/
$dataInicial = "2018-01-01";
$dataFinal = "2019-01-01";
$resultadoEsperado = 365;
$resultado = calculaDias($dataInicial, $dataFinal);
verificaResultado("06", $resultadoEsperado, $resultado);

/***** Teste 07 *****/
$dataInicial = "2018-01-01";
$dataFinal = "2020-01-01";
$resultadoEsperado = 730;
$resultado = calculaDias($dataInicial, $dataFinal);
verificaResultado("07", $resultadoEsperado, $resultado);

/***** Teste 08 *****/
$dataInicial = "2018-12-31";
$dataFinal = "2019-01-01";
$resultadoEsperado = 1;
$resultado = calculaDias($dataInicial, $dataFinal);
verificaResultado("08", $resultadoEsperado, $resultado);

/***** Teste 09 *****/
$dataInicial = "2018-05-31";
$dataFinal = "2018-06-01";
$resultadoEsperado = 1;
$resultado = calculaDias($dataInicial, $dataFinal);
verificaResultado("09", $resultadoEsperado, $resultado);

/***** Teste 10 *****/
$dataInicial = "2018-05-31";
$dataFinal = "2019-06-01";
$resultadoEsperado = 366;
$resultado = calculaDias($dataInicial, $dataFinal);
verificaResultado("10", $resultadoEsperado, $resultado);

/***** Teste 11 *****/
$dataInicial = "2016-02-01";
$dataFinal = "2016-03-01";
$resultadoEsperado = 29;
$resultado = calculaDias($dataInicial, $dataFinal);
verificaResultado("11", $resultadoEsperado, $resultado);

/***** Teste 12 *****/
$dataInicial = "2016-01-01";
$dataFinal = "2016-03-01";
$resultadoEsperado = 60;
$resultado = calculaDias($dataInicial, $dataFinal);
verificaResultado("12", $resultadoEsperado, $resultado);

/***** Teste 13 *****/
$dataInicial = "1981-09-21";
$dataFinal = "2009-02-12";
$resultadoEsperado = 10006;
$resultado = calculaDias($dataInicial, $dataFinal);
verificaResultado("13", $resultadoEsperado, $resultado);

/***** Teste 14 *****/
$dataInicial = "1981-07-31";
$dataFinal = "2009-02-12";
$resultadoEsperado = 10058;
$resultado = calculaDias($dataInicial, $dataFinal);
verificaResultado("14", $resultadoEsperado, $resultado);

/***** Teste 15 *****/
$dataInicial = "2004-03-01";
$dataFinal = "2009-02-12";
$resultadoEsperado = 1809;
$resultado = calculaDias($dataInicial, $dataFinal);
verificaResultado("15", $resultadoEsperado, $resultado);

/***** Teste 16 *****/
$dataInicial = "2004-03-01";
$dataFinal = "2009-02-12";
$resultadoEsperado = 1809;
$resultado = calculaDias($dataInicial, $dataFinal);
verificaResultado("16", $resultadoEsperado, $resultado);

/***** Teste 17 *****/
$dataInicial = "1900-02-01";
$dataFinal = "1900-03-01";
$resultadoEsperado = 28;
$resultado = calculaDias($dataInicial, $dataFinal);
verificaResultado("17", $resultadoEsperado, $resultado);

/***** Teste 18 *****/
$dataInicial = "1899-01-01";
$dataFinal = "1901-01-01";
$resultadoEsperado = 730;
$resultado = calculaDias($dataInicial, $dataFinal);
verificaResultado("18", $resultadoEsperado, $resultado);

/***** Teste 19 *****/
$dataInicial = "2000-02-01";
$dataFinal = "2000-03-01";
$resultadoEsperado = 29;
$resultado = calculaDias($dataInicial, $dataFinal);
verificaResultado("19", $resultadoEsperado, $resultado);

/***** Teste 20 *****/
$dataInicial = "1999-01-01";
$dataFinal = "2001-01-01";
$resultadoEsperado = 731;
$resultado = calculaDias($dataInicial, $dataFinal);
verificaResultado("20", $resultadoEsperado, $resultado);


function verificaResultado($nTeste, $resultadoEsperado, $resultado) {
	if(intval($resultadoEsperado) == intval($resultado)) {
		echo "Teste $nTeste passou.\n";
	} else {
		echo "Teste $nTeste NAO passou (Resultado esperado = $resultadoEsperado, Resultado obtido = $resultado).\n";
	}
}
//aaaa
